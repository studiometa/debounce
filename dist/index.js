"use strict";

Object.defineProperty(exports, "__esModule", {
	value: true
});
/* ================================
 * Debounce
 * ================================ */

/**
 * Returns a function, that, as long as it continues to be invoked, will not
 * be triggered. The function will be called after it stops being called for
 * N milliseconds. If `immediate` is passed, trigger the function on the
 * leading edge, instead of the trailing.
 * @param  {number}    wait       Timer
 * @param  {boolean}   immediate  Launch the function immediately
 * @param  {function}  func       The function that needs debounce
 * @return {function}             A function to bind to the event debounced
 */
var debounce = exports.debounce = function debounce(wait, immediate, func) {
	var timeout = void 0;

	return function () {
		var context = this;
		var args = arguments;
		var later = function later() {
			timeout = null;
			if (!immediate) func.apply(context, args);
		};
		var callNow = immediate && !timeout;
		clearTimeout(timeout);
		timeout = setTimeout(later, wait);
		if (callNow) func.apply(context, args);
	};
};